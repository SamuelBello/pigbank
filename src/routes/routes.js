import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import BuildingPage from '../pages/BuildingConstrution/BuildingPage'
import CotacaoDeMoedasPage from '../pages/CotacaoDeMoedasPage/CotacaoDeMoedasPage'
import HomePage from '../pages/HomePage/HomePage'
import NotFound from '../pages/NotFound/NotFound'

function Routes () {
    return (
        <Router>
            <Switch>
                <Route exact path='/'>
                    <HomePage />
                </Route>
                <Route exact path='/coinPage'>
                    <CotacaoDeMoedasPage />
                </Route>
                <Route exect path='/buildPage'>
                    <BuildingPage />
                </Route>
                <Route exect path='/404' path='/'>
                    <NotFound />
                </Route>
            </Switch>
        </Router>
    )
}

export default Routes