import React from 'react'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'
import './NotFound.css'
import erro404 from '../../assets/erro-404.png'

function NotFound() {
    return (
        <>
            <Header />
            <main>
                <section className="section-1">
                    <h2>Erro 404</h2>
                    <img src={erro404} alt="" />
                    <p>Parece que essa página ainda não foi implementada...<br/>
                        Tente novamente mais tarde!</p>
                    <a href="">VOLTAR</a>
                </section>
            </main>
            <Footer />
        </>
    )
}
export default NotFound
