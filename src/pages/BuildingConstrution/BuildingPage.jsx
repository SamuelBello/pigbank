import React from 'react'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'
import './BuildingPage.css'
import buildingPageImg from '../../assets/building-construction.png'

function BuildingPage() {
    return (
        <>
            <Header />
            <main>
                <section className="section-1">
                    <h2>Em Construção</h2>
                    <img src={buildingPageImg} alt="" />
                    <p>Parece que essa página ainda não foi implementada...<br/>
                        Tente novamente mais tarde!</p>
                    <a href="">VOLTAR</a>
                </section>
            </main>
            <Footer />
        </>
    )
}
export default BuildingPage
