import React from 'react'
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'
import './HomePage.css'
import pigMainImg from '../../assets/pig-main.png'
import coinSectionImg from '../../assets/coin-section.png'

function HomePage() {
    return (
        <>
            <Header />
            <main>
                <section className="section-1">
                    <img src={pigMainImg} alt="" />
                    <div className="container">
                        <h2>A mais nova alternativa de banco digital chegou!</h2>
                        <p>Feito para caber no seu bolso e otimizar
                            seu tempo. O PigBank veio pra ficar</p>
                        <a href=""><h3>Abra a sua conta</h3></a>
                    </div>
                </section>
                <section className="section-2">
                    <div className="container-2">
                        <h2>Fique por dentro!</h2>
                        <p>Ao contrário do ditado popular, por aqui, quem se mistura com porco não come farelo!
                            Conheça nossa plataforma exclusivamente dedicada para ampliar o seu patrimônio.</p>
                        <img src={coinSectionImg} alt="" />
                        <a href=""><h3>Cotação Moedas</h3></a>
                    </div>
                </section>
            </main>
            <Footer />
        </>
    )
}


export default HomePage
