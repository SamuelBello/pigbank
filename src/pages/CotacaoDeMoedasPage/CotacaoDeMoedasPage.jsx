import axios from 'axios'
import React, { useEffect, useState } from 'react'
import Card from '../../components/Card/Card'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'
import {getCoin} from '../../services/api'



function CotacaoDeMoedasPage() {
    const [cotacao, setCotacao] = useState([])
    
    useEffect(() => {
        getCoin(setCotacao)
    }, [])
    
    return (
        <>
            <Header />
                {cotacao.map((coin)=><Card props={coin}/>)}
            <Footer />
        </>
    )
}


export default CotacaoDeMoedasPage
