import React from 'react'
import './Header.css'

function Header() {
    const title = 'Pigbank'

    return (
        <>
            <header>
                <nav>
                    <a href="" class="title"><h1>{title}</h1></a>
                    <div>
                        <a href="" class="menu">Cotação de Moedas</a>
                        <a href="" class="menu">Acessar</a>
                    </div>
                </nav>  
            </header>
        </>
    )
}

export default Header




