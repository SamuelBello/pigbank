import React from 'react'
import './Card.css'

function Card({props}) {
    return (
        <>
            <section className="sectionCoin">
                <div className="coinSign">
                    <div className="vl"></div>
                    <p className="coin">{props['name']}</p>
                    <p className="sign">{props['code']}</p>
                </div>
                <div className="coinValue">
                    <p className="value">Máxima: R$:{props['high']}</p>
                    <p className="value">Mínima: R$:{props['low']}</p>
                    <p className="update">Atualizado em {props['create_date']}</p>
                </div>
            </section>
        </>
    )
}

export default Card
