import React from 'react'
import './style/Reset.css'
import './style/Root.css'
import Routes from './routes/routes';

function App() {
  return (
    <div className="App">
      <Routes />
    </div>
  );
}

export default App;