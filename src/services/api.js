import axios from 'axios';

const create = axios.create({
    baseURL:'https://economia.awesomeapi.com.br/'
})

function getCoin(setMoeda) {
    create.get('/json/all')
    .then((res) => {
        let list = [];
        Object.keys(res.data).forEach(i =>{
            list.push(res.data[i]);
        })
        setMoeda(list)
     })
    .catch((err) => console.error(err));
}

export { getCoin };